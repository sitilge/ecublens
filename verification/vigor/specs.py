#### QUESTIONS ####
# - Where is "state" module defined?
# - Any docs available?
# - How to have multiple maps?

#
from state import flow_emap
#
INT_PORT = 0
#
EXT_PORT = 1
IP = "192.168.0.1"

# Get the header, return on mismatch.
hdrEther = pop_header(ether, on_mismatch=([], []))

# Assert packet received.
assert a_packet_received
# Assert big-endian 0x0800 -> IPv4.
assert hdrEther.type == 8

# Should not dismiss other packets, we need them.
# hdrL4 = pop_header(tcpudp, on_mismatch=([],[]))

if hdrARP:
    if ARPRequest:
        # Assume the ARP request has been sent.
        return ([],[])
    else:
        # Update the ARP table.
        # The key in my ARP hash map is IP address.
        entry_id = FlowIdc(hdrIP.ip_address)
        idx = flow_emap_ARP.get(entry_id)
        flow_emap_ARP.refresh_idx(idx, now)

# No IPv6 allowed beyond this point.
hdrIP = pop_header(ipv4, on_mismatch=([],[]))

if hdrICMP:
    # If the destination IP matches our IP.
    if ip == IP:
        # Send ICMP reply.
        # TODO - should something be added here?
        return ([],[])
    else:
        # Forward downstream.
        # TODO - I'm not sure but such syntax might be similar
        # to what I really need.
        return (
            [INT_PORT],
            [
                ether(hdrEther, saddr=..., daddr=...),
                ipv4(hdrIP, cksum=...)
                icmp(hdrICMP)
            ]
        )

if hdrDHCP:
    if type == Discover:
        # Reserve the IP address.
        idx = a_valid_ip
        flow_emap_DHCP.add(internal_flow_id, idx, now)

        # Send DHCP Offer.
        return ([],[])
    elif type == Offer:
        # Send DHCP Request.
        return ([],[])
    elif type == Request:
        # Assign the IP address.
        idx = the_ip_respective_reserved_ip
        flow_emap_DHCP.add(internal_flow_id, idx, now)

        entry_id = FlowIdc(hdrIP.ip_address)
        idx = flow_emap_ARP.get(entry_id)
        flow_emap_ARP.refresh_idx(idx, now)

    elif type == Acknowledge:
        # Send ARP request.
        return ([],[])

if received_on_port == EXT_PORT:
    #NAT incoming handler.
else:
    #NAT outgoing handler.