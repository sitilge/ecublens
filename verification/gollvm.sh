#!/bin/sh

BASE_DIR=~

# Setup
cd $BASE_DIR
mkdir workarea
cd workarea
git clone https://github.com/llvm/llvm-project.git
cd llvm-project/llvm/tools
git clone https://go.googlesource.com/gollvm
cd gollvm
git clone https://go.googlesource.com/gofrontend
cd libgo
git clone https://github.com/libffi/libffi.git
git clone https://github.com/ianlancetaylor/libbacktrace.git

# Building
cd $BASE_DIR
cd workarea
mkdir build-debug
cd build-debug
cmake -DCMAKE_BUILD_TYPE=Debug -DLLVM_USE_LINKER=gold -DCMAKE_CXX_FLAGS='-fcf-protection=none' -DCMAKE_C_FLAGS='-fcf-protection=none' -G Ninja ../llvm-project/llvm
ninja gollvm