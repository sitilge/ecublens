package router

import (
	"github.com/google/gopacket/layers"
	"github.com/intel-go/nff-go/flow"
	"github.com/intel-go/nff-go/types"
	"strconv"
	"strings"
)

const (
	FlowStop = 0
	FlowSend = 1
)

type Config struct {
	Hostname string
	//TODO - should be moved to private, I guess
	NATTable NATTable

	DHCPTable DHCPTable
	Public    struct {
		ARPTable ARPTable
		Port     Port
	}
	Private struct {
		ARPTable ARPTable
		Ports    []Port
	}
}

type Port struct {
	Index uint16

	//Reference to the parent struct ARPTable, no other way AFAIK...
	ARPTable *ARPTable

	Ethernet struct {
		MAC types.MACAddress
	}

	IP struct {
		AddressAcquired bool
		Address         types.IPv4Address
		Mask            types.IPv4Address
	}

	DHCP struct {
		DiscoveryID     uint32
		LastMessageType layers.DHCPMsgType
	}
}

func ConfigNew(hostname string, public uint, private string) (*Config, error) {
	cfg := &Config{}
	cfg.Hostname = hostname
	cfg.Public.Port.Index = uint16(public)
	cfg.Public.Port.Ethernet.MAC = flow.GetPortMACAddress(uint16(public))
	cfg.Public.ARPTable.Entries = make(map[types.IPv4Address]*ARPEntry)

	privatePorts := strings.Split(private,",")

	for _, privatePort := range privatePorts {
		index, err := strconv.ParseUint(privatePort, 10, 16)
		if err != nil {
			return nil, err
		}

		cfg.Private.Ports = append(cfg.Private.Ports,
			Port{
				Index: uint16(index),
				Ethernet: struct {
					MAC types.MACAddress
				}{
					MAC: flow.GetPortMACAddress(uint16(index)),
				},
				ARPTable: &ARPTable{
					Entries: make(map[types.IPv4Address]*ARPEntry),
				},
			},
		)
	}

	return cfg, nil
}
