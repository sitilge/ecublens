package router

import (
	"github.com/intel-go/nff-go/common"
	"github.com/intel-go/nff-go/packet"
	"github.com/intel-go/nff-go/types"
	"time"
)

const (
	ARPDefaultTTL = 1200
)

type ARPEntry struct {
	MAC types.MACAddress
	TTL uint16
}

type ARPTable struct {
	Entries map[types.IPv4Address]*ARPEntry
}

func ARPStartExpiring(cfg *Config) {
	for {
		public := &cfg.Public.Port
		public.ARPExpireEntries()

		for i := range cfg.Private.Ports {
			private := &cfg.Private.Ports[i]
			private.ARPExpireEntries()
		}

		time.Sleep(1 * time.Second)
	}
}

func (p *Port) ARPUpdateEntry(pkt *packet.Packet) {
	ip := types.ArrayToIPv4(pkt.GetARPNoCheck().SPA)

	entryNew := &ARPEntry{
		MAC: pkt.GetARPNoCheck().SHA,
		TTL: ARPDefaultTTL,
	}

	entryOld, ok := p.ARPTable.Entries[ip]

	if ok {
		//Refresh the existing entry.
		if entryNew.MAC == entryOld.MAC {
			entryOld.TTL = ARPDefaultTTL

			return
		}
	}

	//Insert a new entry.
	p.ARPTable.Entries[ip] = entryNew
}

func (p *Port) ARPExpireEntries() {
	for ip, entry := range p.ARPTable.Entries {
		//Delete expired entries.
		if entry == nil || entry.TTL <= 0 {
			delete(p.ARPTable.Entries, ip)

			continue
		}

		//Send ARP request every other second when below half of the default TTL.
		if entry.TTL < ARPDefaultTTL/2 && entry.TTL%2 == 0 {
			p.ARPSendRequest(ip)
		}

		entry.TTL--
	}
}

func (p *Port) ARPHandle(pkt *packet.Packet) {
	if !p.IP.AddressAcquired {
		return
	}

	//Swapping bytes is important, resolving to false values like 512 (bits = 1_000_000_000)
	if packet.SwapBytesUint16(pkt.GetARPNoCheck().Operation) == packet.ARPReply {
		p.ARPUpdateEntry(pkt)

		return
	}

	SHA := p.Ethernet.MAC
	SPA := p.IP.Address
	THA := pkt.Ether.SAddr
	TPA := types.ArrayToIPv4(pkt.GetARPNoCheck().SPA)

	out, err := packet.NewPacket()
	if err != nil {
		common.LogFatal(common.Debug, err)
	}

	packet.InitARPReplyPacket(out, SHA, THA, SPA, TPA)

	out.SendPacket(p.Index)
}

func (p *Port) ARPSendRequest(ip types.IPv4Address) {
	SHA := p.Ethernet.MAC
	SPA := p.IP.Address
	TPA := ip

	out, err := packet.NewPacket()
	if err != nil {
		common.LogFatal(common.Debug, err)
	}

	packet.InitARPRequestPacket(out, SHA, SPA, TPA)

	out.SendPacket(p.Index)
}
