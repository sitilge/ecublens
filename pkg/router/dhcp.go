package router

import (
	"encoding/binary"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/intel-go/nff-go/common"
	"github.com/intel-go/nff-go/packet"
	"github.com/intel-go/nff-go/types"
	"math/rand"
	"net"
	"time"
)

const (
	DHCPLeaseUnassigned = 0
	DHCPLeaseReserved   = 1
	DHCPLeaseAssigned   = 2
	DHCPServerPort      = 67
	DHCPClientPort      = 68
)

var (
	DHCPBroadcastMAC = [6]uint8{255, 255, 255, 255, 255, 255}
	DHCPBroadcastIP  = []byte{255, 255, 255, 255}
	DHCPGeneralIP    = []byte{0, 0, 0, 0}
	DHCPRouterIP     = []byte{192, 168, 1, 1}
	DHCPSubnetMask   = []byte{255, 255, 255, 0}
	DHCPDefaultTTL   = 86400
)

type DHCPEntry struct {
	MAC      net.HardwareAddr
	Hostname string
	Type     uint8
	TTL      uint64
}

type DHCPTable struct {
	Entries map[types.IPv4Address]*DHCPEntry
}

func DHCPStartClient(cfg *Config) {
	for {
		p := &cfg.Public.Port
		if !p.IP.AddressAcquired {
			//Options are described here: https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol
			options := layers.DHCPOptions{
				//It can be improved by adding option to request a specified IP (e.g. the same as before, custom, etc.).
				layers.NewDHCPOption(layers.DHCPOptMessageType, []byte{byte(layers.DHCPMsgTypeDiscover)}),
				layers.NewDHCPOption(layers.DHCPOptHostname, []byte(cfg.Hostname)),
				layers.NewDHCPOption(layers.DHCPOptParamsRequest, []byte{
					//Copied from Ubuntu 18.04 DHCP Discovery. SubnetMask must come first.
					byte(layers.DHCPOptSubnetMask),
					byte(layers.DHCPOptBroadcastAddr),
					byte(layers.DHCPOptTimeOffset),
					byte(layers.DHCPOptRouter),
					byte(layers.DHCPOptDomainName),
					byte(layers.DHCPOptDNS),
					byte(layers.DHCPOptDomainSearch),
					byte(layers.DHCPOptHostname),
					byte(layers.DHCPOptInterfaceMTU),
				}),
			}

			layer := layers.DHCPv4{
				Operation:    layers.DHCPOpRequest,
				HardwareType: layers.LinkTypeEthernet,
				HardwareLen:  6,
				Xid:          rand.Uint32(),
				ClientHWAddr: p.Ethernet.MAC[:],
				Options:      options,
			}

			p.DHCPSendPacket(&layer)
		}

		//Hardcoded for now.
		time.Sleep(3 * time.Second)
	}
}

func (p *Port) DHCPHandle(cfg *Config, pkt *packet.Packet) uint {
	//Payload contains the  data, which needs to be decoded.
	payload, _ := pkt.GetPacketPayload()

	//Specify which layers to parse by using decoders (variadic), interested in DHCP only in this case.
	var DHCP = layers.DHCPv4{}
	parser := gopacket.NewDecodingLayerParser(layers.LayerTypeDHCPv4, &DHCP)
	var decoded []gopacket.LayerType
	err := parser.DecodeLayers(payload, &decoded)

	//Send on errors or multiple decoded.
	if err != nil || len(decoded) != 1 {
		return FlowSend
	}

	//The DHCP packet has been decoded, message type is one of the options.
	messageType := DHCPGetOption(&DHCP, layers.DHCPOptMessageType)

	//Forward non-DHCP packets.
	if messageType == nil {
		return FlowSend
	}

	//Switch and check if the DHCP DORA sequence is not violated.
	switch messageType.Data[0] {
	case byte(layers.DHCPMsgTypeDiscover):
		if p.DHCP.LastMessageType == layers.DHCPMsgTypeUnspecified {
			p.DHCPHandleDiscover(cfg, &DHCP)
		}
	case byte(layers.DHCPMsgTypeOffer):
		if p.DHCP.LastMessageType == layers.DHCPMsgTypeDiscover {
			p.DHCPHandleOffer(cfg, &DHCP)
		}
	case byte(layers.DHCPMsgTypeRequest):
		//TODO - sometimes got stuck here because the message types are instantly Request
		//TODO - without going the full cycle... Should there be a counter for failed tries for hosts?
		//TODO - and then reset the type?
		if p.DHCP.LastMessageType == layers.DHCPMsgTypeOffer {
			p.DHCPHandleRequest(cfg, &DHCP)
		}
	case byte(layers.DHCPMsgTypeAck):
		if p.DHCP.LastMessageType == layers.DHCPMsgTypeRequest {
			p.DHCPHandleAcknowledge(&DHCP)
		}
	default:
		return FlowSend
	}

	return FlowStop
}

func DHCPReserveLease(cfg *Config, dhcp *layers.DHCPv4) types.IPv4Address {
	var ip types.IPv4Address

	hostname := DHCPGetOption(dhcp, layers.DHCPOptHostname)

	entry := &DHCPEntry{
		MAC:      dhcp.ClientHWAddr,
		Hostname: string(hostname.Data),
		Type:     DHCPLeaseReserved,
	}

	if cfg.DHCPTable.Entries == nil {
		cfg.DHCPTable.Entries = make(map[types.IPv4Address]*DHCPEntry)
	}

	for i := byte(2); i < 255; i++ {
		testing := types.BytesToIPv4(192, 168, 1, i)

		_, ok := cfg.DHCPTable.Entries[testing]

		if !ok {
			cfg.DHCPTable.Entries[testing] = entry

			ip = testing

			break
		}
	}

	return ip
}

func DHCPAssignLease(cfg *Config, ip types.IPv4Address) *DHCPEntry {
	entry, ok := cfg.DHCPTable.Entries[ip]

	//Assign the lease.
	if ok {
		entry.Type = DHCPLeaseAssigned
		entry.TTL = uint64(DHCPDefaultTTL)
	}

	return entry
}

func (p *Port) DHCPHandleDiscover(cfg *Config, dhcp *layers.DHCPv4) {
	ip := DHCPReserveLease(cfg, dhcp)

	if ip == 0 {
		return
	}

	leaseTime := make([]byte, 4)
	binary.BigEndian.PutUint32(leaseTime, uint32(DHCPDefaultTTL))

	options := layers.DHCPOptions{
		layers.NewDHCPOption(layers.DHCPOptMessageType, []byte{byte(layers.DHCPMsgTypeOffer)}),
		layers.NewDHCPOption(layers.DHCPOptSubnetMask, DHCPSubnetMask),
		layers.NewDHCPOption(layers.DHCPOptRouter, DHCPRouterIP),
		layers.NewDHCPOption(layers.DHCPOptLeaseTime, leaseTime),
		layers.NewDHCPOption(layers.DHCPOptServerID, DHCPRouterIP),
	}

	layer := layers.DHCPv4{
		Operation:    layers.DHCPOpReply,
		YourClientIP: net.ParseIP(ip.String()).To4(),
		HardwareType: layers.LinkTypeEthernet,
		HardwareLen:  6,
		Xid:          dhcp.Xid,
		ClientHWAddr: dhcp.ClientHWAddr,
		Options:      options,
	}

	p.DHCPSendPacket(&layer)
}

func (p *Port) DHCPHandleOffer(cfg *Config, dhcp *layers.DHCPv4) {
	options := layers.DHCPOptions{
		layers.NewDHCPOption(layers.DHCPOptMessageType, []byte{byte(layers.DHCPMsgTypeRequest)}),
		layers.NewDHCPOption(layers.DHCPOptHostname, []byte(cfg.Hostname)),
		layers.NewDHCPOption(layers.DHCPOptServerID, dhcp.NextServerIP),
		layers.NewDHCPOption(layers.DHCPOptRequestIP, dhcp.YourClientIP),
	}

	layer := layers.DHCPv4{
		Operation:    layers.DHCPOpRequest,
		HardwareType: layers.LinkTypeEthernet,
		HardwareLen:  6,
		Xid:          dhcp.Xid,
		ClientHWAddr: dhcp.ClientHWAddr,
		Options:      options,
	}

	p.DHCPSendPacket(&layer)
}

func (p *Port) DHCPHandleRequest(cfg *Config, dhcp *layers.DHCPv4) {
	ip := DHCPGetOption(dhcp, layers.DHCPOptRequestIP)

	lease := DHCPAssignLease(cfg, types.SliceToIPv4(ip.Data))

	//Return if no valid lease has been found.
	if lease.Type == DHCPLeaseUnassigned {
		return
	}

	leaseTime := make([]byte, 4)
	binary.BigEndian.PutUint32(leaseTime, uint32(DHCPDefaultTTL))

	options := layers.DHCPOptions{
		layers.NewDHCPOption(layers.DHCPOptMessageType, []byte{byte(layers.DHCPMsgTypeAck)}),
		layers.NewDHCPOption(layers.DHCPOptServerID, DHCPRouterIP),
		layers.NewDHCPOption(layers.DHCPOptLeaseTime, leaseTime),
		layers.NewDHCPOption(layers.DHCPOptSubnetMask, DHCPSubnetMask),
		layers.NewDHCPOption(layers.DHCPOptRouter, DHCPRouterIP),
		layers.NewDHCPOption(layers.DHCPOptDomainName, []byte(cfg.Hostname)),
		layers.NewDHCPOption(layers.DHCPOptDNS, DHCPRouterIP),
	}

	layer := layers.DHCPv4{
		Operation:    layers.DHCPOpReply,
		YourClientIP: ip.Data,
		HardwareType: layers.LinkTypeEthernet,
		HardwareLen:  6,
		Xid:          dhcp.Xid,
		ClientHWAddr: dhcp.ClientHWAddr,
		Options:      options,
	}

	p.DHCPSendPacket(&layer)

	//Update the ARP table.
	MAC, err := types.StringToMACAddress(dhcp.ClientHWAddr.String())
	if err != nil {
		common.LogFatal(common.Debug, err)
	}

	cfg.Private.ARPTable.Entries[types.SliceToIPv4(ip.Data)] = &ARPEntry{
		MAC: MAC,
		TTL: ARPDefaultTTL,
	}
}

func (p *Port) DHCPHandleAcknowledge(dhcp *layers.DHCPv4) {
	option := DHCPGetOption(dhcp, layers.DHCPOptSubnetMask)

	//Set the IP address and mask.
	p.IP.AddressAcquired = true
	p.IP.Address = types.SliceToIPv4(dhcp.YourClientIP.To4())
	p.IP.Mask = types.SliceToIPv4(option.Data)

	common.LogDebug(common.Debug, "Successfully acquired IP address:", p.IP.Address, "on port:", p.Index)
	common.LogDebug(common.Debug, "Successfully acquired mask:", p.IP.Mask, "on port:", p.Index)

	//DHCP RFC 2131 does not require ARP RFC 5227. It is only "should"...
	//Ask router for it's MAC.
	p.ARPSendRequest(types.SliceToIPv4(DHCPRouterIP))
}

func (p *Port) DHCPSendPacket(dhcp *layers.DHCPv4) {
	//Package gopacket offers functionality for serialization.
	payload := gopacket.NewSerializeBuffer()
	opts := gopacket.SerializeOptions{
		FixLengths:       true,
		ComputeChecksums: true,
	}
	err := gopacket.SerializeLayers(payload, opts, dhcp)
	if err != nil {
		common.LogFatal(common.Debug, err)
	}

	//Create a new packet. May fail if the flows have not been initialized.
	out, err := packet.NewPacket()
	if err != nil {
		common.LogFatal(common.Debug, err)
	}

	//Init a new UDP IP packet and set the payload size.
	packet.InitEmptyIPv4UDPPacket(out, uint(len(payload.Bytes())))

	//Copy the contents of the serialized payload to the packet.
	load, _ := out.GetPacketPayload()
	copy(load, payload.Bytes())

	//L2 layer setup, mutual for request and response.
	out.Ether.SAddr = p.Ethernet.MAC

	if dhcp.Operation == layers.DHCPOpRequest {
		//L2 layer setup.
		out.Ether.DAddr = DHCPBroadcastMAC

		//L3 layer setup. No need to check one more time for IP.
		out.GetIPv4NoCheck().SrcAddr = types.SliceToIPv4(DHCPGeneralIP)
		out.GetIPv4NoCheck().DstAddr = types.SliceToIPv4(DHCPBroadcastIP)

		//L4 layer setup. No need to check one more time for UDP.
		out.GetUDPNoCheck().SrcPort = packet.SwapBytesUint16(DHCPClientPort)
		out.GetUDPNoCheck().DstPort = packet.SwapBytesUint16(DHCPServerPort)
	} else {
		//L2 layer setup.
		MAC, err := types.StringToMACAddress(dhcp.ClientHWAddr.String())
		if err != nil {
			common.LogDebug(common.Debug, err)
		}
		out.Ether.DAddr = MAC

		//L3 layer setup. No need to check one more time for IP.
		out.GetIPv4NoCheck().SrcAddr = types.SliceToIPv4(DHCPRouterIP)
		out.GetIPv4NoCheck().DstAddr = types.SliceToIPv4(dhcp.YourClientIP)

		//L4 layer setup. No need to check one more time for UDP.
		out.GetUDPNoCheck().SrcPort = packet.SwapBytesUint16(DHCPServerPort)
		out.GetUDPNoCheck().DstPort = packet.SwapBytesUint16(DHCPClientPort)
	}

	//Calculate checksums.
	ChecksumsCalculateUDP(out)

	//Save the type of the last message for preventing DORA violation.
	option := DHCPGetOption(dhcp, layers.DHCPOptMessageType)
	p.DHCP.LastMessageType = layers.DHCPMsgType(option.Data[0])

	out.SendPacket(p.Index)
}

func DHCPGetOption(DHCP *layers.DHCPv4, option layers.DHCPOpt) *layers.DHCPOption {
	for i := range DHCP.Options {
		if DHCP.Options[i].Type == option {
			return &DHCP.Options[i]
		}
	}

	return nil
}

func DHCPStartExpiring(cfg *Config) {
	for {
		DHCPExpireEntries(cfg)

		time.Sleep(1 * time.Second)
	}
}

func DHCPExpireEntries(cfg *Config) {
	for ip, entry := range cfg.DHCPTable.Entries {
		//Delete expired entries.
		if entry.TTL <= 0 {
			delete(cfg.DHCPTable.Entries, ip)

			continue
		}

		entry.TTL--
	}
}
