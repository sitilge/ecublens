package router

import (
	"github.com/intel-go/nff-go/packet"
	"github.com/intel-go/nff-go/types"
	"unsafe"
)

func ChecksumsCalculateTCP(pkt *packet.Packet) {
	//Calculate L3 checksums.
	l3 := pkt.GetIPv4NoCheck()
	l3.HdrChecksum = packet.SwapBytesUint16(packet.CalculateIPv4Checksum(l3))

	//Calculate L4 checksums.
	l4 := pkt.GetTCPNoCheck()
	data := unsafe.Pointer(uintptr(unsafe.Pointer(l4)) + types.TCPMinLen)
	l4.Cksum = packet.SwapBytesUint16(packet.CalculateIPv4TCPChecksum(l3, l4, data))
}

func ChecksumsCalculateUDP(pkt *packet.Packet) {
	//Calculate L3 checksums.
	l3 := pkt.GetIPv4NoCheck()
	l3.HdrChecksum = packet.SwapBytesUint16(packet.CalculateIPv4Checksum(l3))

	//Calculate L4 checksums.
	l4 := pkt.GetUDPNoCheck()
	data := unsafe.Pointer(uintptr(unsafe.Pointer(l4)) + uintptr(types.UDPLen))
	l4.DgramCksum = packet.SwapBytesUint16(packet.CalculateIPv4UDPChecksum(l3, l4, data))
}

func ChecksumsCalculateICMP(pkt *packet.Packet) {
	//Calculate L3 checksums.
	l3 := pkt.GetIPv4NoCheck()
	l3.HdrChecksum = packet.SwapBytesUint16(packet.CalculateIPv4Checksum(l3))

	//Calculate L4 checksums.
	l4 := pkt.GetICMPNoCheck()
	data := unsafe.Pointer(uintptr(unsafe.Pointer(l4)) + types.ICMPLen)
	pkt.GetICMPNoCheck().Cksum = packet.SwapBytesUint16(packet.CalculateIPv4ICMPChecksum(l3, l4, data))
}
