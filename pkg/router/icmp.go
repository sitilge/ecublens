package router

import (
	"github.com/intel-go/nff-go/common"
	"github.com/intel-go/nff-go/packet"
	"github.com/intel-go/nff-go/types"
)

func (p *Port) ICMPHandle(pkt *packet.Packet) uint {
	//Stop if IP has not been acquired.
	if !p.IP.AddressAcquired {
		return FlowStop
	}

	//Forward packets not addressed to us.
	if pkt.GetIPv4NoCheck().DstAddr != p.IP.Address {
		return FlowSend
	}

	//Generate a new packet.
	out, err := packet.NewPacket()
	if err != nil {
		common.LogFatal(common.Debug, err)
	}

	//Copy the contents of the old packet to the new one.
	packet.GeneratePacketFromByte(out, pkt.GetRawPacketBytes())

	//Parse L3 and L4 headers, required as it has not been done yet.
	out.ParseL3()
	out.ParseL4ForIPv4()

	out.Ether.SAddr = pkt.Ether.DAddr
	out.Ether.DAddr = pkt.Ether.SAddr

	out.GetIPv4NoCheck().SrcAddr = pkt.GetIPv4NoCheck().DstAddr
	out.GetIPv4NoCheck().DstAddr = pkt.GetIPv4NoCheck().SrcAddr
	out.GetIPv4NoCheck().HdrChecksum = packet.SwapBytesUint16(packet.CalculateIPv4Checksum(out.GetIPv4NoCheck()))

	out.GetICMPNoCheck().Type = types.ICMPTypeEchoResponse
	ChecksumsCalculateICMP(out)

	out.SendPacket(p.Index)

	return FlowStop
}
