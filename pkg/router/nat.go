package router

import (
	"github.com/intel-go/nff-go/packet"
	"github.com/intel-go/nff-go/types"
)

const (
	NATDefaultTTL = 1200
)

type NATEntry struct {
	SrcAddr  types.IPv4Address
	SrcPort  uint16
	DstAddr  types.IPv4Address
	DstPort  uint16
	Protocol int
	TTL      uint64
}

type NATTable struct {
	Entries []NATEntry
}

func (p *Port) NATHandleIncoming(cfg *Config, pkt *packet.Packet) uint {
	for _, entry := range cfg.NATTable.Entries {
		if (entry.Protocol == types.TCPNumber &&
			entry.SrcPort == pkt.GetTCPNoCheck().DstPort &&
			entry.DstAddr == pkt.GetIPv4NoCheck().SrcAddr &&
			entry.DstPort == pkt.GetTCPNoCheck().SrcPort) ||
			(entry.Protocol == types.UDPNumber &&
				entry.SrcPort == pkt.GetUDPNoCheck().DstPort &&
				entry.DstAddr == pkt.GetIPv4NoCheck().SrcAddr &&
				entry.DstPort == pkt.GetUDPNoCheck().SrcPort) {
			//Check if there is an entry in the ARP table as well.
			arp, ok := cfg.Private.ARPTable.Entries[entry.SrcAddr]

			if !ok {
				//Dropping, if not in the ARP table.
				return FlowStop
			}

			pkt.Ether.SAddr = cfg.Public.Port.Ethernet.MAC
			pkt.Ether.DAddr = arp.MAC
			pkt.GetIPv4NoCheck().DstAddr = entry.SrcAddr

			//TODO - can fail here, messing with headers...
			if pkt.GetTCPNoCheck() != nil {
				ChecksumsCalculateTCP(pkt)
			} else {
				ChecksumsCalculateUDP(pkt)
			}

			return FlowSend
		}
	}

	return FlowStop
}

func (p *Port) NATHandleOutgoing(cfg *Config, pkt *packet.Packet) uint {
	pkt.Ether.SAddr = cfg.Public.Port.Ethernet.MAC

	dstMAC, ok := cfg.Public.ARPTable.Entries[types.SliceToIPv4(DHCPRouterIP)]

	if !ok {
		//The node that wanted to send the packet is not in the ARP table, so
		//drop the packet for now and send ARP first, will pass on next
		//packet retransmissions.
		cfg.Public.Port.ARPSendRequest(types.SliceToIPv4(DHCPRouterIP))

		return FlowStop
	}

	entry := NATEntry{
		SrcAddr: pkt.GetIPv4NoCheck().SrcAddr,
		DstAddr: pkt.GetIPv4NoCheck().DstAddr,
		TTL:     NATDefaultTTL,
	}

	pkt.Ether.DAddr = dstMAC.MAC
	pkt.GetIPv4NoCheck().SrcAddr = cfg.Public.Port.IP.Address

	if pkt.GetTCPNoCheck() != nil {
		ChecksumsCalculateTCP(pkt)

		entry.SrcPort = pkt.GetTCPNoCheck().SrcPort
		entry.DstPort = pkt.GetTCPNoCheck().DstPort
		entry.Protocol = types.TCPNumber
	} else {
		ChecksumsCalculateUDP(pkt)

		entry.SrcPort = pkt.GetUDPNoCheck().SrcPort
		entry.DstPort = pkt.GetUDPNoCheck().DstPort
		entry.Protocol = types.UDPNumber
	}

	cfg.NATTable.Entries = append(cfg.NATTable.Entries, entry)

	return FlowSend
}
