package main

import (
	"github.com/intel-go/nff-go/examples/nffPktgen/generator"
	"github.com/intel-go/nff-go/flow"
	"time"
)

func main() {
	configuration, err := generator.ReadConfig("config.json")
	flow.CheckFatal(err)
	context, err := generator.GetContext(configuration)
	flow.CheckFatal(err)
	outFlow, _, err := flow.SetFastGenerator(generator.Generate, 1000, context)
	flow.CheckFatal(err)
	flow.CheckFatal(flow.SetSender(outFlow, uint16(0)))

	// periodically print statistics
	go func() {
		g :=  generator.GetGenerator()
		for {
			println("Sent", g.GetGeneratedNumber(), "packets")
			time.Sleep(time.Second * 5)
		}
	}()
}