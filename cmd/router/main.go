package main

import (
	"flag"
	"git.sitilge.id.lv/sitilge/ecublens/pkg/router"
	"github.com/intel-go/nff-go/common"
	"github.com/intel-go/nff-go/flow"
	"github.com/intel-go/nff-go/packet"
	"github.com/intel-go/nff-go/types"
	"math/rand"
	"os"
	"os/signal"
	"time"
)

const (
	DirectionPublicToPrivate = 0
	DirectionPrivateToPublic = 1
)

type FlowContext struct {
	Config    *router.Config
	Port      *router.Port
	Direction uint8
}

func main() {
	hostname := flag.String("hostname", "ecublens", "Hostname for DHCP")
	public := flag.Uint("public", 0, "Public interface")
	private := flag.String("private", "1", "Private interfaces, comma-separated")
	flag.Parse()

	rand.Seed(time.Now().UnixNano())

	//Should be called before graph construction.
	flow.CheckFatal(flow.SystemInit(nil))

	//Initialize ports.
	cfg, err := router.ConfigNew(*hostname, *public, *private)
	if err != nil {
		common.LogFatal(common.Debug, err)
	}

	//Construct the flow graphs.
	InitFlows(cfg)

	flow.CheckFatal(flow.SystemInitPortsAndMemory())

	//Start the DHCP client.
	go func() {
		router.DHCPStartClient(cfg)
	}()

	//Start expiring the DHCP table.
	go func() {
		router.DHCPStartExpiring(cfg)
	}()

	//Start expiring the NAT table.
	go func() {
		router.ARPStartExpiring(cfg)
	}()

	//Start the flow scheduler.
	go func() {
		flow.CheckFatal(flow.SystemStartScheduler())
	}()

	//Wait for SIGINT to happen.
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
}

func InitFlows(cfg *router.Config) {
	//Initialize public to private flow.
	public := &cfg.Public.Port
	public.ARPTable = &cfg.Public.ARPTable

	//Receive packets on the public interface.
	publicToPrivateReceiver, err := flow.SetReceiver(public.Index)
	flow.CheckFatal(err)
	flowPublic := NewFlowContext(cfg, public, DirectionPublicToPrivate)

	//Split into many flows (flow 0 is reserved for stopper), stop the current flow.
	countPrivatePorts := uint(len(cfg.Private.Ports)) + 1
	publicToPrivateFlows, err := flow.SetSplitter(publicToPrivateReceiver, HandleFlow, countPrivatePorts, flowPublic)
	flow.CheckFatal(err)

	//Stop packets from the new flow 0.
	flow.CheckFatal(flow.SetStopper(publicToPrivateFlows[0]))

	for i := range cfg.Private.Ports {
		//Initialize private to public flow.
		private := &cfg.Private.Ports[i]
		private.ARPTable = &cfg.Private.ARPTable

		private.IP.Address = types.SliceToIPv4(router.DHCPRouterIP)
		private.IP.AddressAcquired = true

		//Receive packets on the private interface.
		privateToPublicReceiver, err := flow.SetReceiver(private.Index)
		flow.CheckFatal(err)
		flowPrivate := NewFlowContext(cfg, private, DirectionPrivateToPublic)

		//Split into 2 flows (flow 0 is reserved for stopper), stop the current flow.
		countPublicPorts := uint(2)
		privateToPublicTranslation, err := flow.SetSplitter(privateToPublicReceiver, HandleFlow, countPublicPorts, flowPrivate)
		flow.CheckFatal(err)

		//Stop packets from the new flow 0.
		flow.CheckFatal(flow.SetStopper(privateToPublicTranslation[0]))

		flow.CheckFatal(flow.SetSender(privateToPublicTranslation[private.Index], public.Index))

		flow.CheckFatal(flow.SetSender(publicToPrivateFlows[1], private.Index))
	}
}

func HandleFlow(pkt *packet.Packet, ctx flow.UserContext) uint {
	flw := ctx.(FlowContext)

	//Return parsed L3 level headers.
	hdrIPv4, _, hdrARP := pkt.ParseAllKnownL3()

	//Check if at least one protocol is parsed.
	if hdrIPv4 == nil && hdrARP == nil {
		return router.FlowStop
	}

	//Handle ARP.
	if hdrARP != nil {
		flw.Port.ARPHandle(pkt)

		return router.FlowStop
	}

	//Parse L4 level headers for IP.
	var hdrTCP = &packet.TCPHdr{}
	hdrTCP, hdrUDP, hdrICMP := pkt.ParseAllKnownL4ForIPv4()

	//Check if at least one protocol is parsed.
	if hdrTCP == nil && hdrUDP == nil && hdrICMP == nil {
		return router.FlowStop
	}

	//Handle ICMP.
	if hdrICMP != nil {
		return flw.Port.ICMPHandle(pkt)
	}

	//Handle DHCP.
	if hdrUDP != nil {
		return flw.Port.DHCPHandle(flw.Config, pkt)
	}

	//Handle NAT.
	if ctx.(FlowContext).Direction == DirectionPublicToPrivate {
		return flw.Port.NATHandleIncoming(flw.Config, pkt)
	} else {
		return flw.Port.NATHandleOutgoing(flw.Config, pkt)
	}
}

func NewFlowContext(cfg *router.Config, port *router.Port, direction uint8) *FlowContext {
	return &FlowContext{
		Config:    cfg,
		Port:      port,
		Direction: direction,
	}
}

func (f FlowContext) Copy() interface{} {
	return FlowContext{
		Config:    f.Config,
		Port:      f.Port,
		Direction: f.Direction,
	}
}

func (f FlowContext) Delete() {
}
