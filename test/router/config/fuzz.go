// +build gofuzz

package config

import (
	"git.sitilge.id.lv/sitilge/ecublens/pkg/router"
	"strconv"
)

func Fuzz(data []byte) int {
	hostname := string(data)

	public, err := strconv.ParseUint(string(data), 10, 0)
	if err != nil {
		return 0
	}

	private := string(data)

	_, err = router.ConfigNew(hostname, uint(public), private)
	if err != nil {
		return 0
	}

	return 1
}