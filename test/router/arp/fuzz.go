package arp

import (
	"fmt"
	"git.sitilge.id.lv/sitilge/ecublens/pkg/router"
	"github.com/google/gofuzz"
	"github.com/intel-go/nff-go/common"
	"github.com/intel-go/nff-go/flow"
	"github.com/intel-go/nff-go/types"
)

var (
	port router.Port
)

func init() {
	cf := flow.Config{
		//DPDKArgs: []string{"-m 12000"},
		//DPDKArgs: []string{"--in-memory", "--no-shconf", "--no-huge", "-m 12000"},
		DPDKArgs: []string{"--proc-type=auto"},
		//DPDKArgs: []string{"--proc-type=auto", "--log-level=5", "-m 4096"},
		//DPDKArgs: []string{"--in-memory", "--no-shconf", "-m 8192", "--iova-mode=pa"},
		//DPDKArgs: []string{"--in-memory", "--no-shconf", "--no-huge"},
		//DPDKArgs: []string{"--no-huge", "-m 8192"},
		//DPDKArgs: []string{"--log-level=2"},
	}

	flow.CheckFatal(flow.SystemInit(&cf))

	//TODO - so many restarts (1/10) and few execs(2500new/sec)...
	temp, _ := flow.SetReceiver(1)
	flow.CheckFatal(flow.SetStopper(temp))
	flow.CheckFatal(flow.SystemInitPortsAndMemory())

	cfg, err := router.ConfigNew("ecublens", 0, "1")
	if err != nil {
		common.LogFatal(common.Debug, err)
	}

	//Take the first private port, it really doesn't matter.
	port = cfg.Private.Ports[0]
}

func FuzzARPExpireEntries(data []byte) int {
	fuzz.NewFromGoFuzz(data).Fuzz(&port.ARPTable.Entries)

	entries := make(map[types.IPv4Address]uint16, 0)

	//Copy all contents for the incoming reference.
	for ip, entry := range port.ARPTable.Entries {
		if entry == nil {
			//flow.CheckFatal(flow.SystemReset())
			flow.SystemReset()

			return 0
		}

		entries[ip] = entry.TTL
	}

	//Run the function.
	port.ARPExpireEntries()

	//Check for invalid outcomes.
	for ip, entry := range port.ARPTable.Entries {
		if entry.TTL < 0 {
			ttl := entries[ip]

			panic(fmt.Sprintf("%s %v \n", "entry exists with invalid TTL < 0, original input TTL was", ttl))
		}
	}

	//flow.CheckFatal(flow.SystemStop())
	flow.SystemReset()

	return 1
}
