#!/bin/sh

#A unified script for running the fuzzing functions.

case $1 in

expire-entries|expire-entries/)
  GO_FUZZ_BUILD_FUNC=FuzzARPExpireEntries
  GO_FUZZ_BUILD_OUTPUT=./expire-entries/expire-entries.zip
  GO_FUZZ_WORKDIR=expire-entries
  GO_FUZZ_BIN=$GO_FUZZ_BUILD_OUTPUT
  ;;
update-entry|update-entry/)
  GO_FUZZ_BUILD_FUNC=FuzzARPUpdateEntry
  GO_FUZZ_BUILD_OUTPUT=./update-entry/update-entry.zip
  GO_FUZZ_WORKDIR=update-entry
  GO_FUZZ_BIN=$GO_FUZZ_BUILD_OUTPUT
  ;;
*)
  echo "Invalid function provided"
  exit 1
  ;;

esac

# Source the scripts file.
source ~/scripts.sh

# Disable ASLR.
disable_aslr

# Bind the interfaces.
devbind_bind

#Execute commands with the provided flags.
go-fuzz-build -func $GO_FUZZ_BUILD_FUNC -o $GO_FUZZ_BUILD_OUTPUT

if [ $? -eq 0 ]; then
  sudo rm -rf $GO_FUZZ_WORKDIR/corpus
  sudo rm -rf $GO_FUZZ_WORKDIR/crashers
  sudo rm -rf $GO_FUZZ_WORKDIR/suppressions

  sudo go-fuzz -workdir=$GO_FUZZ_WORKDIR -bin=$GO_FUZZ_BIN

  sudo chown -R vagrant:vagrant ./*
fi