# ecublens

A home router based on the NFF-Go framework. Currently 5 virtual network functions are supported:

- [x] ARP
- [x] IPv4
- [x] ICMP
- [x] DHCP
- [x] NAT

## Installation

To install and run Ecublens on a fresh Arch Linux:
- Install `sudo`, `git`, `grub`, `gcc`, `libnuma`, `liblua`, `libpcap`.
- By convenience, the installation happens in user's home directory, thus `cd ~`.
- Clone the NFF-Go repo `git clone --recurse-submodules https://github.com/intel-go/nff-go`.
- Build NFF-Go `cd nff-go && go mod download && make -j8 NFF_GO_NO_MLX_DRIVERS=1 NFF_GO_NO_BPF_SUPPORT=1`.
- Clone the Ecublens repo `git clone https://git.sitilge.id.lv/sitilge/ecublens.git`.
- Copy and then configure the scripts file containing environment variables and functions `cp ./ecublens/tools/vagrant/machines/ecublens/scripts.sh ~`. In most cases `lspci` gives enough information to setup correct `NFF_GO_CARDS` values.
- Run `ecublens_run` to launch the router.