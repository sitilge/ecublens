# Testing

## Description

This is a testbed for the `ecublens` router. Testing is based on the following network topology (in descending order of routing):

![Network topology](network_topology.png "Network topology")

- `pfsense` - the entry router based on [pfsense](https://www.pfsense.org) with SSH enabled for remote packet capture from LAN. As regards interfaces, WAN is using NAT, LAN has two interfaces for `ecublens` and `a2` both connected to the internal network `a`. The web interface can be accessed from LAN using `192.168.1.1`. The default user and password for both web and SSH is `root:pfsense`.

- `ecublens` - the home router to be tested. The machine is based on [Arch Linux](https://www.archlinux.org), plus the essential packages and kernel params, drivers and hugepages to support DPDK. `ecublens` is based on [nff-go](https://github.com/intel-go/nff-go) with support for Mellanox cards and BPF currently disabled as they out of the scope of the project. The `~/scripts.sh` hold some useful env variables and functions for DPDK setup and building and running `ecublens`, for example, to bind the interfaces and build and run, simply run `ecublens_run` function. Interface wise, WAN is connected to the internal network `a`, while two LAN interfaces are available for the internal network `b`, for use in nodes `b1` and `b2` respectively. As a side note, `virtio` paravirtualized adapters available with VirtualBox are much faster in the initialization stage of DPDK. This particular setup requires at least 10 CPU cores to run as there are 3 flow functions for each interfaces and 1 core is reserved for the scheduler, thus 3 (WAN) + 2 * 3 (LAN) + 1 (scheduler) = 10 cores. In the context of the internal network `a`, one can think of `ecublens` as `a1`. To update, disable change current WAN interface from internal network to NAT, and pull the updates from the Internet. The default user and password is `vagrant:vagrant`.

- `a2` - a support node for the internal network `a`, based on [Ubuntu](https://www.ubuntu.com), plus extra GUI tools installed. They are used for testing the functionality of `ecublens` NF WAN. This node can also be used to capture packets and configure `pfsense`. Packet capture is done using the `packet_capture` function, which brings up a `wireshark` instance and starts capturing all packets on `pfsense` interface `em0`. The default user and password is `vagrant:vagrant`.

- `b1` and `b2` - support nodes for the internal network `b`, based on [Ubuntu](https://www.ubuntu.com), plus extra GUI tools installed. They are used for testing the functionality of `ecublens` NF LAN. The default user and password is `vagrant:vagrant`.

## Usage

Make sure `virtualbox` is installed on your system. If you cloned the `ecublens` repo without `--recurse-submodules` flag, then run `git submodule update --init --recursive`, which will download all the submodules, including the VirtualBox machines. The total size of the machines is about 7GB. Then the easiest way to start is simply to run:

````
cd virtualbox/scripts
./import.sh
./start.sh
````

The scripts imports and starts the pre-made VirtualBox virtual machines (`pfsense`, `ecublens`, `a2`, `b1`, `b2`), which have all the system configuration, network topology, etc. already preconfigured.

The VirtualBox virtual machines themselves are built using Vagrant and then making some hardware level adjustments, which are easier to be done using VirtualBox without involving Vagrant. Make sure you have `vagrant` installed on your system. Some machines require the `vagrant-reload`, which can be installed by running `vagrant plugin install vagrant-reload`. Then run the virtual machine (`ecublens` for `ecublens`, `node` for `a2`, `b1` and `b2`, or `pfsense` for `pfsense`) using `vagrant up` command.

## Todo

- [ ] update the `ecublens` machine to using 10 cores and `virtio` adapters, currently using only `b1` and 7 cores.
- [ ] On support nodes, disable screen lock, add wireshark, terminal and network tabs to favourite bar, remove redundant for convenience, add Baidu 104.193.88.77 for testing
- [ ] `pfsense` and maybe `ecublens` should be run in headless mode.
- [ ] Support nodes should be running Ubuntu.