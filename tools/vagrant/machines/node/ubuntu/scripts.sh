#!/bin/bash

# Capture remote packets from the default gateway.
remote_capture() {
  ssh-copy-id root@192.168.1.1
  wireshark -k -i <(ssh root@192.168.1.1 tcpdump -i em1 -U -w - not tcp port 22)
}

# General user aliases.
alias ..="cd ./../"
alias ...="cd ./../../"
