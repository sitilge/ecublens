#!/bin/bash

echo "Provision base1 started"

# Generate SSH keys for remote packet capture.
ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa

echo "Provision base1 finished"
