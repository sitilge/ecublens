#!/bin/bash

echo "Provision base0 started"

# Install the essential official packages.
apt-get -y install wireshark-qt vim

# Add user to wireshark group.
gpasswd -a vagrant wireshark

# Source the scripts.
echo ". ~/scripts.sh" >> .bashrc

echo "Provision base0 finished"
