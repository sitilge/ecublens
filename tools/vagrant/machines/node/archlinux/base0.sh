#!/bin/bash

echo "Provision base0 started"

# Install the essential official packages.
pacman -Syyu --noconfirm --needed base base-devel git xorg xorg-xinit xfce4 firefox wireshark-qt vim

# Add user to wireshark group.
gpasswd -a vagrant wireshark

# Source the scripts.
echo ". ~/scripts.sh" >> .bashrc

echo "Provision base0 finished"
