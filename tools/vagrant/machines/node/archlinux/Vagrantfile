Vagrant.configure("2") do |config|
    # Define some variables, configurable from env.
    vm_gui = ENV.fetch("VM_GUI", false)
    vm_cpu_node = ENV.fetch("VM_CPU_NODE", 4)
    vm_mem_node = ENV.fetch("VM_MEM_NODE", 4 * 1024)

    config.vm.provider "virtualbox" do |vb|
        # Set the name of the VirtualBox VM.
        vb.name = "node"

        # Customize the number of CPUs available.
        vb.cpus = "#{vm_cpu_node}"

        # Customize the amount of memory on the VM.
        vb.memory = "#{vm_mem_node}"

        if #{vm_gui}
            # Display the VirtualBox GUI when booting the machine.
            vb.gui = true

            # Set the video memory to 32M.
            vb.customize ["modifyvm", :id, "--vram", "32"]
        end

        # Disable the Virtual Remote Display Extension.
        vb.customize ["modifyvm", :id, "--vrde", "off"]
    end

    # Select the box.
    config.vm.box = "archlinux/archlinux"

    # Disable the synced folder.
    config.vm.synced_folder ".", "/vagrant", disabled: true

    # Set the hostname of the VM.
    config.vm.hostname = "node"

    # Set the shell.
    config.ssh.shell = "bash"

    # Default network topology.
    config.vm.network "private_network", type: "dhcp", virtualbox__intnet: "a", auto_config: false

    # Provision the OS.
    config.vm.provision "shell", path: "base0.sh"
    config.vm.provision "shell", path: "base1.sh", privileged: false
    config.vm.provision "file", source: "scripts.sh", destination: "scripts.sh"
    config.vm.provision "file", source: ".xinitrc", destination: ".xinitrc"

    # Use the default SSH credentials.
    # See this bug: https://github.com/hashicorp/vagrant/issues/5186
    config.ssh.username = "vagrant"
    config.ssh.password = "vagrant"
    config.ssh.insert_key = true
end