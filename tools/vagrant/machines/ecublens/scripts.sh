#!/bin/sh

# General NFF-Go and DPDK exports.
export NFF_GO=$HOME/nff-go
export NFF_GO_CARDS="00:03.0 00:08.0 00:09.0"
export PATH=$HOME/go/bin:$PATH

# Flags for CGO.
export RTE_TARGET=x86_64-native-linuxapp-gcc
DPDK_DIR=dpdk
DPDK_INSTALL_DIR=${RTE_TARGET}-install
export RTE_SDK=${NFF_GO}/dpdk/${DPDK_DIR}/${DPDK_INSTALL_DIR}/usr/local/share/dpdk
export CGO_LDFLAGS_ALLOW='-Wl,--((no-)?whole-archive|((start|end)-group))'
export CGO_CFLAGS="-I${RTE_SDK}/${RTE_TARGET}/include -O3 -std=gnu11 -m64 -pthread -march=native -mno-fsgsbase -mno-f16c -DRTE_MACHINE_CPUFLAG_SSE -DRTE_MACHINE_CPUFLAG_SSE2 -DRTE_MACHINE_CPUFLAG_SSE3 -DRTE_MACHINE_CPUFLAG_SSSE3 -DRTE_MACHINE_CPUFLAG_SSE4_1 -DRTE_MACHINE_CPUFLAG_SSE4_2 -DRTE_MACHINE_CPUFLAG_PCLMULQDQ -DRTE_MACHINE_CPUFLAG_RDRAND -DRTE_MACHINE_CPUFLAG_F16C -include rte_config.h -Wno-deprecated-declarations"
export CGO_LDFLAGS="-L${RTE_SDK}/${RTE_TARGET}/lib -Wl,--no-as-needed -Wl,-export-dynamic"

# Bind ports from Linux kernel to DPDK driver.
devbind_bind() {
  sudo $NFF_GO/dpdk/dpdk/usertools/dpdk-devbind.py --force --bind=uio_pci_generic $NFF_GO_CARDS
}

# Unbind ports from DPDK to Linux kernel driver.
devbind_unbind() {
  sudo $NFF_GO/dpdk/dpdk/usertools/dpdk-devbind.py --unbind $NFF_GO_CARDS
}

# Display status.
devbind_status() {
  sudo $NFF_GO/dpdk/dpdk/usertools/dpdk-devbind.py --status
}

# Enable ASLR.
disable_aslr() {
  sudo sysctl -w kernel.randomize_va_space=0
}

# Disable ASLR.
enable_aslr() {
  sudo sysctl -w kernel.randomize_va_space=2
}


# Run the ecublens router.
ecublens_run() {
  # Disable ASLR.
  disable_aslr

  # Bind the interfaces.
  devbind_bind

  cd ~/ecublens/cmd/router

  # Build and run the router.
  go build && sudo ./router
}

# General user aliases.
alias ..="cd ./../"
alias ...="cd ./../../"
alias l="ls -lht"
alias ll="ls -laht"
