#!/bin/bash

echo "Provision base2 started"

# Install AUR wrapper.
git clone https://aur.archlinux.org/trizen.git
(
  cd trizen
  makepkg --noconfirm --needed -sric
  rm -rf trizen
)

# Install utilities for hugepages.
trizen -Syu --noconfirm --needed libhugetlbfs

# Build and install NFF-Go.
go get github.com/golang/protobuf/protoc-gen-go
git clone --recursive https://github.com/intel-go/nff-go
(
  cd nff-go
  go mod download
  make -j8 NFF_GO_NO_MLX_DRIVERS=1 NFF_GO_NO_BPF_SUPPORT=1
)

# Clone the project itself.
git clone https://git.sitilge.id.lv/sitilge/ecublens
(
  cd ecublens/src
  go get
)

# Source the scripts.
echo ". ~/scripts.sh" >> .bashrc

echo "Provision base2 finished"
