#!/bin/sh

echo "Provision base0 started"

# Pick the best mirror available.
pacman -S --noconfirm --needed reflector
reflector -p http --save /etc/pacman.d/mirrorlist

# Refresh mirrors and install essential official packages.
pacman -Syyu --noconfirm --needed base base-devel linux-headers lua git go numactl protobuf neovim the_silver_searcher

# If pse exists, 2M hugepages are supported; if pdpe1gb exists, 1G hugepages are supported.
# Apparently, the Vagrant Arch Linux kernel supports 2MB only.
sed -i '/GRUB_CMDLINE_LINUX_DEFAULT/c\GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet hugepagesz=2MB hugepages=2048"' /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg

# Load a DPDK supported Linux driver.
echo uio_pci_generic >>/etc/modules-load.d/uio-pci-generic.conf

echo "Provision base0 finished"
