#!/bin/sh

echo "Provision base1 started"

# Mount hugetables.
mkdir /mnt/huge
mount -t hugetlbfs nodev /mnt/huge

echo "Provision base1 finished"
