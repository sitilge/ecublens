#!/bin/sh

#An alternative solution to hugepages.
echo 1024 | sudo tee /proc/sys/vm/nr_hugepages

#An alternative solution to DPDK drivers.
sudo modprobe uio_pci_generic