#!/bin/sh

vboxmanage import --options keepallmacs ./../machines/pfsense.ova
vboxmanage import --options keepallmacs ./../machines/ecublens.ova
vboxmanage import --options keepallmacs ./../machines/a2.ova
vboxmanage import --options keepallmacs ./../machines/b1.ova
vboxmanage import --options keepallmacs ./../machines/b2.ova
