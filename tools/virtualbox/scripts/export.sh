#!/bin/sh

vboxmanage export pfsense -o ./../machines/pfsense.ova
vboxmanage export ecublens -o ./../machines/ecublens.ova
vboxmanage export a2 -o ./../machines/a2.ova
vboxmanage export b1 -o ./../machines/b1.ova
vboxmanage export b2 -o ./../machines/b2.ova
