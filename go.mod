module git.sitilge.id.lv/sitilge/ecublens

go 1.14

require (
	github.com/dvyukov/go-fuzz v0.0.0-20200318091601-be3528f3a813 // indirect
	github.com/google/gofuzz v1.1.1-0.20200317195917-c89cefbc28dc
	github.com/google/gopacket v1.1.17
	github.com/intel-go/nff-go v0.9.2
)
