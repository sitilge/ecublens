12 Notes

12.1 Introduction
With off-the-shelf computers becoming more performant, the reasons why choose a dedicated ASIC-based router over an NF-based router are fading out.

It is possible for units to communicate using message passing, for example, when deployed as microservices or shared memory.

the common-sense is to perceive them as units providing certain functions management. On the other hand, development time is much longer and bugs are hard to find -

I have fuzzy tested the code using two tools: go-fuzz and gofuzz.

Modern languages, such as Go, gives the best of both worlds by offering comparable performance to C while substantially improving development speed and type safety.

I such as buffer overflows, dangling pointers, concurrency, speed of development, tooling, etc.

focus on speed of development and maintainability, as well as

NFs are deployed on Unix-like operating systems like Linux and BSD where kernel, libraries and user-space utilities are heavily using C [#torvaldsTorvaldsLinux2020].

Yet even though testing software is de-facto standard in software development, the techniques and tools of testing differ. being a developer competent in building NFs does not automatically mean being interested in testing. NFs being an integral part of the network naturally [#martinsClickOSArtNetwork] requires proof of correctness, however verification should put a negligible burden to developers.

There are reasons which make ASIC-based home routers an interesting target for research and conversion to verifiable NFs: 1) Improved flexibility - users no longer dependant on vendor updates or hardware. 2) Deployment costs - NFs do not require expensive hardware, virtualization is possible. 3) Sandboxing - the scope is more local when compared to operator environments, which is the usual deployment environment [#martinsClickOSArtNetwork].

There are three parts this project will focus on - NFs, verification, and home routers. The problem is to identify which NFs makes a simple, yet fully functional home router, develop the missing NFs and verify the correctness. This project will build on the results (e.g., a NAT and a firewall NFs) presented in the earlier papers [#pirelliFormallyVerifiedNAT2018, #zaostrovnykhFormallyVerifiedNAT2017, #zaostrovnykhVerifyingSoftwareNetwork2019].

12.2 Design

Another aspect is the direction and origin of the packet

There is a flaw in the definition as it merely considers IP packets

ARP is the first supported protocol.

Currently Ecublens support 5 protocols:

• OSI model level 2 (data link) -

It offers TUI for console users and web-based GUI for nodes in the same LAN. Although such functionality may not belong NFs in their commonly seen way, they provide monitoring and configuration services.

Martins et al. described some of more than 300 elements available for ClickOS.

There are router firmwares, both open-sourced (OpnSense) and closed source (pfSense), offering Even before starting thinking about the design

• Chicken egg problem

• ClickOS 21 NFs

• Flow graph here

• Flows

• About each protocol?

• Tell about each of the firmwares

12.3 Deployment

To update, disable change current WAN interface from internal network to NAT, and pull the updates from the Internet.

13 Goals and Challenges

The main goals for the project are:

1. Identify essential NFs required for a home router.

2. Design, build and evaluate the missing NFs.

3. Write specifications based on RFCs, IEEE standards, etc.

4. Verify the NFs against given specifications.

5. Add functionality to the existing Vigor toolchain if required.

Some of the challenges are:

1. Finding the set of NFs is highly subjective for each use case.

2. Writing NFs such as IPS might be challenging.

3. Toolchain (libVig) lacks some primitives for cypto and regexp [#zaostrovnykhVerifyingSoftwareNetwork2019].

4. NFs are not always documented, e.g. for monitoring and configuration.

5. A good understanding of the exact NF and the whole stack is required.

6. Verification might take infinite time if done incorrectly, e.g. path explosion.

7. Previous verification required significant system resources for full stack (700 GB RAM, 28 cores, 1 hour clock time) [#zaostrovnykhVerifyingSoftwareNetwork2019].

8. Bugs lower in the stack (e.g. DPDK, ixgbe) might interfere with NFs.

14 Approach

Initial work will consist of reading related sources to specify the core NFs of a home router. ClickOS as of now holds 21 elements (NFs), which might be used as inspiration. Elements are written in C++ thus a rewrite to C will be required. Another aspect that has to be taken into account is documentation which, if exists, will define the behavior of the NF, regardless of existing implemenentations. Vigor toolchain will be used to build and run each NF - libVig for data structures such as expiring hashmaps and number range manager, DPDK as the IO framework, Intel ixgbe as the network driver, and NFOS as the custom built operating system. Python will be used for writing specifications. Aside from some specifics required by Vigor, the toolchain should not interfere much with the development process. Each NF will be extensively verified; in case of failure debug information will be provided by Vigor and the respective code snippet will be updated accordingly. Some NFs evaluation terms will be: 1) Performance - what are the results (e.g. latency, throughput) for each NF? How does it compare to other NF implementations? 2) Verification metrics - how long does it take to verify a particular NF with or without the underlying software stack?